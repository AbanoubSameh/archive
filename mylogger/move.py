import os
import sys

new_directory = "copy/"
new_file_name = "/move.py"

current_directory = "/"+sys.argv[0]
current_file_name = os.path.dirname(os.path.realpath(__file__))

current_file = open(current_file_name+current_directory, "r")
new_file = open(new_directory+new_file_name, "w")

for i in current_file.read():
    new_file.write(i)

current_file.close()
new_file.close()
